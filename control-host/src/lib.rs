use pyo3::{
    conversion::{IntoPy, ToPyObject},
    create_exception,
    exceptions::{
        PyAttributeError, PyBrokenPipeError, PyException, PyLookupError, PyTypeError, PyValueError,
    },
    pycell::PyCell,
    pyclass, pyfunction, pymethods, pymodule,
    types::{PyDict, PyList, PyModule, PyTuple},
    wrap_pyfunction, Py, PyAny, PyErr, PyObject, PyResult, Python,
};

use serde::{
    de::{self, DeserializeSeed},
    ser::{SerializeStructVariant, SerializeTuple},
    Serialize, Serializer,
};

use tokio::{
    io::{
        AsyncBufRead, AsyncBufReadExt, AsyncReadExt, AsyncWriteExt, BufReader, BufWriter, ReadHalf,
        WriteHalf,
    },
    runtime,
    sync::{mpsc, oneshot},
};

use std::{
    collections::HashMap,
    fmt::{self, Display, Write},
    fs, io,
    ops::{Index, IndexMut},
    sync::{Arc, Mutex},
};

use control_proto::{self as proto, Msg};
use crc::{Crc, CRC_32_CKSUM};
use elf_rs::{Elf, ElfFile};
use lazy_static::lazy_static;
use postcard::{flavors::SerFlavor, from_bytes, serialize_with_flavor, take_from_bytes};
use postcard_cobs::{decode_in_place, encode, max_encoding_length};
use pyo3_asyncio::tokio::future_into_py;
use tokio_serial::SerialStream;

lazy_static! {
    static ref RT: runtime::Runtime = {
        runtime::Builder::new_current_thread()
            .enable_io()
            .build()
            .unwrap()
    };
    static ref PORT_INIT: mpsc::Sender<PortInit> = {
        let (tx, mut rx) = mpsc::channel(1);
        std::thread::spawn(move || {
            RT.block_on(async move {
                while let Some(port_init) = rx.recv().await {
                    tokio::spawn(PortMaster::drive(port_init));
                }
            });
        });

        tx
    };
}

#[pyclass(module = "control_host")]
struct Device {
    schema: Py<Schema>,
    tx: mpsc::Sender<RequestCmd>,
}

#[pyclass(module = "control_host")]
struct Schema {
    items: HashMap<Arc<str>, Item>,
    crc32: u32,
}

enum Item {
    Module(Py<Module>),
    Type(Py<NamedType>),
}

impl ToPyObject for Item {
    fn to_object(&self, py: Python) -> PyObject {
        match self {
            Item::Module(object) => object.to_object(py),
            Item::Type(object) => object.to_object(py),
        }
    }
}

#[pyclass(module = "control_host", name = "Type")]
struct NamedType {
    ty: Arc<Type>,
}

#[pyclass(module = "control_host")]
struct Synthetic {
    ty: Arc<Type>,
    value: Dyn,
}

#[pyclass(module = "control_host")]
struct Module {
    name: Arc<str>,
    cmds: HashMap<Arc<str>, Py<Command>>,
}

#[pyclass(module = "control_host")]
struct BoundModule {
    module: Py<Module>,
    device: Py<Device>,
}

#[pyclass(module = "control_host")]
struct BoundCommand {
    cmd: Py<Command>,
    device: Py<Device>,
}

#[pyclass(module = "control_host")]
struct Command {
    params: Vec<Param>,
    ret: Arc<Type>,
    mod_index: u32,
    cmd_index: u32,
}

struct Param {
    name: String,
    ty: Arc<Type>,
}

enum Type {
    Bool,
    U8,
    U16,
    U32,
    Tuple(Vec<Arc<Type>>),
    Struct {
        name: String,
        fields: Vec<(String, Arc<Type>)>,
    },
    Array(Arc<Type>, u32),
    Option(Arc<Type>),
    I32,
}

impl Display for Type {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        use Type::*;

        match self {
            Bool => fmt.write_str("bool"),
            U8 => fmt.write_str("u8"),
            U16 => fmt.write_str("u16"),
            U32 => fmt.write_str("u32"),
            I32 => fmt.write_str("i32"),

            Tuple(items) => {
                fmt.write_char('(')?;

                if let Some(first) = items.first() {
                    Display::fmt(first, fmt)?;
                }

                for item in items.iter().skip(1) {
                    write!(fmt, ", {item}")?;
                }

                fmt.write_char(')')
            }

            Struct { name, .. } => fmt.write_str(name),
            Array(of, length) => write!(fmt, "[{of}; {length}]"),
            Option(of) => write!(fmt, "Option<{of}>"),
        }
    }
}

struct PortInit {
    port: SerialStream,
    crc32: u32,
    tx: oneshot::Sender<PyResult<mpsc::Sender<RequestCmd>>>,
}

enum RequestCmd {
    New {
        tx: oneshot::Sender<PyResult<Packet>>,
    },
    Send {
        tx: oneshot::Sender<PyResult<Packet>>,
        request: Packet,
    },
}

struct Packet {
    buf: Buffer,
    header: Msg<()>,
    body_offset: usize,
}

impl Packet {
    fn body(&self) -> &[u8] {
        let buf = self.buf.as_ref();
        &buf[buf.len().min(self.body_offset)..]
    }
}

#[derive(Clone)]
enum Dyn {
    Bool(bool),
    U8(u8),
    U16(u16),
    U32(u32),
    I32(i32),
    //TODO: This should be Vec<>, blocked by fn call_async() and DynOf.
    // Once this is a Vec, remove #[derive(Clone)] and `.value.clone()`.
    Product(Arc<[Dyn]>),
    Option(Option<Arc<Dyn>>),
}

create_exception!(control_host, NoDeviceError, PyLookupError);
create_exception!(control_host, ProtocolError, PyException);
create_exception!(control_host, ImageError, PyException);

#[pymodule]
fn control_host(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(image, m)?)?;
    m.add_function(wrap_pyfunction!(open_serial, m)?)?;

    Ok(())
}

#[pyfunction]
fn image(py: Python, path: &str) -> PyResult<Py<Schema>> {
    //FIXME: unbound read
    let image = fs::read(path)?;
    let elf = Elf::from_bytes(&image)
        .map_err(|err| ImageError::new_err(format!("ELF error: {:?}", err)))?;

    let schema_bytes = elf
        .lookup_section(b".control_device")
        .ok_or_else(|| {
            ImageError::new_err("Schema not found (missing `#[derive(ControlDevice)]`?)")
        })?
        .content();

    let read_schema = match ReadSchema::new(schema_bytes, py) {
        Ok(read_schema) => read_schema,

        Err(HeaderError::Invalid) => {
            return Err(ImageError::new_err("Bad schema header"));
        }

        Err(HeaderError::Version(version)) => {
            let built = proto::ProtoVersion::BUILT;
            let err = format!("Incompatible schema: got v{version}, expected v{built}");

            return Err(ImageError::new_err(err));
        }
    };

    let crc32 = read_schema.crc32();
    let items = read_schema.into_items()?;

    Py::new(py, Schema { crc32, items })
}

struct ReadSchema<'a> {
    blob: &'a [u8],
    offset: usize,
    types_blob: &'a [u8],
    modules_blob: &'a [u8],
    next_mod: u32,
    ids: HashMap<u32, Arc<Type>>,
    items: HashMap<Arc<str>, Item>,
    py: Python<'a>,
}

enum HeaderError {
    Invalid,
    Version(proto::ProtoVersion),
}

impl<'a> ReadSchema<'a> {
    fn new(bytes: &'a [u8], py: Python<'a>) -> Result<Self, HeaderError> {
        if bytes.len() < 12 {
            return Err(HeaderError::Invalid);
        }

        let modules_start = u32_at(bytes, 4) as usize;
        let schema_end = u32_at(bytes, 8) as usize;

        if modules_start < 12 || modules_start > schema_end || schema_end > bytes.len() {
            return Err(HeaderError::Invalid);
        }

        let version = proto::ProtoVersion {
            major: bytes[0],
            minor: bytes[1],
            patch: bytes[2],
        };

        if version != proto::ProtoVersion::BUILT {
            return Err(HeaderError::Version(version));
        }

        Ok(ReadSchema {
            blob: &bytes[..schema_end],
            offset: 12,
            types_blob: &bytes[12..modules_start],
            modules_blob: &bytes[modules_start..schema_end],
            next_mod: 0,
            py,
            ids: Default::default(),
            items: Default::default(),
        })
    }

    fn crc32(&self) -> u32 {
        Crc::<u32>::new(&CRC_32_CKSUM).checksum(self.blob)
    }

    fn into_items(mut self) -> PyResult<HashMap<Arc<str>, Item>> {
        self.extract_types()?;
        self.extract_modules()?;

        Ok(self.items)
    }

    fn extract_types(&mut self) -> PyResult<()> {
        let mut start = 0;
        while start < self.types_blob.len() {
            let (length, _) = self.extract_type_at(self.offset + start)?;
            start += length;
        }

        Ok(())
    }

    fn bad_types() -> PyErr {
        ImageError::new_err("Corrupt type graph")
    }

    fn extract_ref(&mut self, id: u32) -> PyResult<Arc<Type>> {
        self.extract_type_at(id as usize)
            .map(|(_, ty)| Arc::clone(ty))
    }

    fn extract_type_at(&mut self, at: usize) -> PyResult<(usize, &Arc<Type>)> {
        if at < self.offset || at - self.offset > self.types_blob.len() {
            return Err(Self::bad_types());
        }

        let start = at as usize - self.offset;

        let remaining = self.types_blob.len() - start;
        if remaining < 4 {
            return Err(Self::bad_types());
        }

        let length = u32_at(self.types_blob, start) as usize;
        let total_length = 4 + length;

        if length == 0 || start + total_length > self.types_blob.len() {
            return Err(Self::bad_types());
        }

        // A borrowck limitation tends to appear here
        let ty = self.ids.get(&(at as u32)).is_none().then(|| {
            match (length, self.types_blob[start + 4]) {
                (1, 0) => Ok(Type::Bool),
                (1, 1) => Ok(Type::U8),
                (1, 2) => Ok(Type::U16),
                (1, 3) => Ok(Type::U32),
                (1, 8) => Ok(Type::I32),

                (_, 4) => {
                    let items = (0..((length - 1) / 4))
                        .map(|i| self.extract_ref(u32_at(self.types_blob, start + 4 + 1 + 4 * i)))
                        .collect::<Result<_, _>>()?;

                    Ok(Type::Tuple(items))
                }

                (_, 5) => {
                    let mut blob = &self.types_blob[start + 4 + 1..start + total_length];

                    let name = take_str(&mut blob)
                        .map_err(|()| Self::bad_types())?
                        .to_owned();

                    let fields = std::iter::from_fn(|| {
                        (!blob.is_empty()).then(|| {
                            let name = take_str(&mut blob).map_err(|()| Self::bad_types())?;
                            let id = take_u32(&mut blob).map_err(|()| Self::bad_types())?;
                            let ty = self.extract_ref(id)?;

                            let ok: PyResult<_> = Ok((name, ty));
                            ok
                        })
                    })
                    .collect::<Result<_, _>>()?;

                    Ok(Type::Struct { name, fields })
                }

                (9, 6) => {
                    let of = self.extract_ref(u32_at(self.types_blob, start + 4 + 1))?;
                    let length = u32_at(self.types_blob, start + 4 + 1 + 4);

                    Ok(Type::Array(of, length))
                }

                (5, 7) => {
                    let of = self.extract_ref(u32_at(self.types_blob, start + 4 + 1))?;
                    Ok(Type::Option(of))
                }

                _ => return Err(Self::bad_types()),
            }
        });

        let ty = ty
            .transpose()?
            .map(|ty| {
                let ty = Arc::new(ty);
                let name = match &*ty {
                    Type::Struct { name, .. } => Some(name),
                    _ => None,
                };

                if let Some(name) = name {
                    let named = NamedType {
                        ty: Arc::clone(&ty),
                    };

                    let item = Item::Type(Py::new(self.py, named)?);
                    self.items.insert(name.as_str().into(), item);
                }

                let ok: PyResult<_> = Ok(ty);
                ok
            })
            .transpose()?;

        let ty = self.ids.entry(at as u32).or_insert_with(|| ty.unwrap());
        Ok((total_length, ty))
    }

    fn extract_modules(&mut self) -> PyResult<()> {
        while !self.modules_blob.is_empty() {
            let (name, module) = self.next_module()?;
            self.items.insert(name, Item::Module(module));
        }

        Ok(())
    }

    fn next_module(&mut self) -> PyResult<(Arc<str>, Py<Module>)> {
        let name = self.mod_take_str()?.into();
        let num_cmds = self.mod_take_u32()?;

        let mut cmds = HashMap::new();
        for cmd_index in 0..num_cmds {
            let name = self.mod_take_str()?.into();
            let ret = self.mod_take_ty()?;

            let num_params = self.mod_take_u32()?;
            let params = (0..num_params)
                .map(|_| {
                    let name = self.mod_take_str()?;
                    let ty = self.mod_take_ty()?;

                    Ok(Param { name, ty })
                })
                .collect::<Result<_, PyErr>>()?;

            let cmd = Command {
                params,
                ret,
                mod_index: self.next_mod,
                cmd_index,
            };

            cmds.insert(name, Py::new(self.py, cmd)?);
        }

        self.next_mod += 1;
        let module = Module {
            name: Arc::clone(&name),
            cmds,
        };

        Ok((name, Py::new(self.py, module)?))
    }

    fn mod_take_str(&mut self) -> PyResult<String> {
        take_str(&mut self.modules_blob).map_err(|()| Self::image_truncated())
    }

    fn mod_take_ty(&mut self) -> PyResult<Arc<Type>> {
        let id = self.mod_take_u32()?;
        self.ids
            .get(&id)
            .cloned()
            .ok_or_else(|| ImageError::new_err(format!("Undefined typeref: {id}")))
    }

    fn mod_take_u32(&mut self) -> PyResult<u32> {
        take_u32(&mut self.modules_blob).map_err(|()| Self::image_truncated())
    }

    fn image_truncated() -> PyErr {
        ImageError::new_err("Corrupt or truncated schema")
    }
}

fn u32_at(bytes: &[u8], at: usize) -> u32 {
    u32::from_le_bytes((&bytes[at..at + 4]).try_into().unwrap())
}

fn take_str(blob: &mut &[u8]) -> Result<String, ()> {
    let length = take_u32(blob)? as usize;
    if length > blob.len() {
        return Err(());
    }

    let (string, next) = blob.split_at(length);
    let string = String::from_utf8(string.to_owned()).map_err(drop)?;

    *blob = next;
    Ok(string)
}

fn take_u32(blob: &mut &[u8]) -> Result<u32, ()> {
    if blob.len() < 4 {
        return Err(());
    }

    let (bytes, next) = blob.split_at(4);
    *blob = next;

    Ok(u32::from_le_bytes(bytes.try_into().unwrap()))
}

#[pyfunction]
fn open_serial(schema: Py<Schema>, port: &str, baud_rate: u32) -> PyResult<Py<Device>> {
    let _guard = RT.enter();
    let builder = tokio_serial::new(port, baud_rate);

    let port = match SerialStream::open(&builder) {
        Ok(port) => port,
        Err(err) => {
            use tokio_serial::ErrorKind::*;
            let err = match err.kind {
                NoDevice => NoDeviceError::new_err(err.description),
                InvalidInput => PyValueError::new_err(err.description),
                Io(kind) => io::Error::new(kind, err).into(),
                _ => io::Error::new(io::ErrorKind::Other, err).into(),
            };

            return Err(err);
        }
    };

    let crc32 = Python::with_gil(|py| schema.try_borrow(py).map(|schema| schema.crc32))?;
    let (tx, rx) = oneshot::channel();

    ok_or_broken(PORT_INIT.blocking_send(PortInit { port, crc32, tx }))?;

    let cmd_tx = ok_or_broken(rx.blocking_recv())??;
    Python::with_gil(|py| Py::new(py, Device { schema, tx: cmd_tx }))
}

#[pymethods]
impl Device {
    fn __getattr__(self_: Py<Self>, py: Python, attr: &str) -> PyResult<PyObject> {
        let this = self_.try_borrow(py)?;
        let schema = this.schema.try_borrow(py)?;
        let item = schema.__getattr__(py, attr)?;

        let item = match item.as_ref(py).downcast::<PyCell<Module>>() {
            Err(_) => item,

            Ok(module) => {
                drop(schema);
                drop(this);

                let bound = BoundModule {
                    module: module.into(),
                    device: self_,
                };

                Py::new(py, bound)?.into_py(py)
            }
        };

        Ok(item)
    }
}

#[pymethods]
impl BoundModule {
    fn __getattr__(&self, py: Python, attr: &str) -> PyResult<Py<BoundCommand>> {
        let cmd = self.module.try_borrow(py)?.__getattr__(attr)?;
        let cmd = BoundCommand {
            cmd,
            device: Py::clone(&self.device),
        };

        Py::new(py, cmd)
    }
}

#[pymethods]
impl Schema {
    fn __getattr__(&self, py: Python, attr: &str) -> PyResult<PyObject> {
        self.items
            .get(attr)
            .ok_or_else(|| {
                let err = format!("Target schema defines no item '{attr}'");
                PyAttributeError::new_err(err)
            })
            .map(|item| item.to_object(py))
    }
}

#[pymethods]
impl NamedType {
    #[args(kwargs = "**")]
    fn __call__(&self, py: Python, kwargs: Option<&PyDict>) -> PyResult<Py<Synthetic>> {
        let value = match &*self.ty {
            Type::Struct { fields, .. } => {
                let fields = fields.iter().map(|(name, ty)| (name.as_str(), &**ty));
                Dyn::Product(match_args(fields, kwargs)?.into())
            }

            _ => {
                let err = format!("Type '{}' is not call-constructible", self.ty);
                return Err(PyTypeError::new_err(err));
            }
        };

        let synthetic = Synthetic {
            ty: Arc::clone(&self.ty),
            value,
        };

        Py::new(py, synthetic)
    }
}

#[pymethods]
impl Synthetic {
    fn __getattr__(&self, py: Python, name: &str) -> PyResult<PyObject> {
        let attr = match (&self.value, &*self.ty) {
            (Dyn::Product(values), Type::Struct { fields, .. }) => fields
                .iter()
                .enumerate()
                .find(|(_i, (field_name, _ty))| field_name == name)
                .map(|(i, (_name, ty))| DynOf(values[i].clone(), ty).into_py(py)),

            _ => None,
        };

        attr.ok_or_else(|| {
            let err = format!("This synthetic has no attribute '{name}'");
            PyAttributeError::new_err(err)
        })
    }

    fn __repr__(&self) -> String {
        format!("{}", DynRepr(&self.value, &self.ty))
    }
}

#[pymethods]
impl Module {
    fn __getattr__(&self, attr: &str) -> PyResult<Py<Command>> {
        self.cmds
            .get(attr)
            .ok_or_else(|| {
                let name = &self.name;
                let err = format!("Module '{name}' defines no method '{attr}'");
                PyAttributeError::new_err(err)
            })
            .map(Py::clone)
    }
}

#[pymethods]
impl Command {
    #[args(kwargs = "**")]
    fn __call__(
        self_: Py<Self>,
        py: Python,
        device: Py<Device>,
        kwargs: Option<Py<PyDict>>,
    ) -> PyResult<&PyAny> {
        Self::call_async(py, self_, device, kwargs)
    }
}

#[pymethods]
impl BoundCommand {
    #[args(kwargs = "**")]
    fn __call__<'a>(&self, py: Python<'a>, kwargs: Option<Py<PyDict>>) -> PyResult<&'a PyAny> {
        Command::call_async(py, Py::clone(&self.cmd), Py::clone(&self.device), kwargs)
    }
}

impl Command {
    fn call_async(
        py: Python,
        self_: Py<Self>,
        device: Py<Device>,
        kwargs: Option<Py<PyDict>>,
    ) -> PyResult<&PyAny> {
        future_into_py(py, Self::call(self_, device, kwargs))
    }

    async fn call(
        self_: Py<Self>,
        device: Py<Device>,
        kwargs: Option<Py<PyDict>>,
    ) -> PyResult<PyObject> {
        let (args, tx) = Python::with_gil::<_, PyResult<_>>(|py| {
            let self_ = self_.as_ref(py).try_borrow()?;
            let device = device.as_ref(py).try_borrow()?;
            let kwargs = kwargs.as_ref().map(|kwargs| kwargs.as_ref(py));

            let params = self_
                .params
                .iter()
                .map(|param| (param.name.as_str(), &*param.ty));
            let args = match_args(params, kwargs)?;

            Ok((args, device.tx.clone()))
        })?;

        let mut request = Self::transaction(&tx, None).await?;
        Python::with_gil(|py| {
            let self_ = self_.as_ref(py).try_borrow()?;
            self_.fill_request(&mut request, &args)
        })?;

        let response = Self::transaction(&tx, Some(request)).await?;
        Python::with_gil(|py| deserialize_typed(py, &response, &self_.as_ref(py).try_borrow()?.ret))
    }

    async fn transaction(
        tx: &mpsc::Sender<RequestCmd>,
        request: Option<Packet>,
    ) -> PyResult<Packet> {
        let (reply_tx, reply_rx) = oneshot::channel();
        let msg = match request {
            None => RequestCmd::New { tx: reply_tx },
            Some(request) => RequestCmd::Send {
                request,
                tx: reply_tx,
            },
        };

        ok_or_broken(tx.send(msg).await)?;
        ok_or_broken(reply_rx.await)?
    }

    fn fill_request(&self, request: &mut Packet, args: &[Dyn]) -> PyResult<()> {
        struct MsgVariant<'a> {
            cmd: &'a Command,
            args: &'a [Dyn],
        }

        struct ModVariant<'a> {
            cmd: &'a Command,
            args: &'a [Dyn],
        }

        impl Serialize for MsgVariant<'_> {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
            where
                S: Serializer,
            {
                let variant = ModVariant {
                    cmd: self.cmd,
                    args: self.args,
                };

                serializer.serialize_newtype_variant("", self.cmd.mod_index, "", &variant)
            }
        }

        impl Serialize for ModVariant<'_> {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
            where
                S: Serializer,
            {
                let mut serializer = serializer.serialize_struct_variant(
                    "",
                    self.cmd.cmd_index,
                    "",
                    self.args.len(),
                )?;

                for arg in self.args.iter() {
                    serializer.serialize_field("", arg)?;
                }

                serializer.end()
            }
        }

        let msg = MsgVariant { cmd: self, args };
        append_raw(&msg, request.buf.as_mut()).map_err(postcard_err)
    }
}

fn match_args<'a, I>(params: I, kwargs: Option<&PyDict>) -> PyResult<Vec<Dyn>>
where
    I: Clone + ExactSizeIterator<Item = (&'a str, &'a Type)>,
{
    let mut args = Vec::new();
    args.resize(params.len(), None);

    for (key, value) in kwargs.into_iter().flat_map(PyDict::iter) {
        let key: &str = key.extract()?;
        let (index, _) = params
            .clone()
            .enumerate()
            .find(|(_, (name, _ty))| *name == key)
            .ok_or_else(|| PyTypeError::new_err(format!("Unknown parameter '{key}'")))?;

        args[index] = Some(value);
    }

    args.into_iter()
        .zip(params)
        .map(|(arg, (name, ty))| {
            arg.ok_or_else(|| PyTypeError::new_err(format!("Missing parameter: '{name}'")))
                .and_then(|arg| Dyn::extract(arg, ty))
        })
        .collect()
}

struct PortMaster {
    rx: BufReader<ReadHalf<SerialStream>>,
    tx: BufWriter<WriteHalf<SerialStream>>,
    cmd_rx: mpsc::Receiver<RequestCmd>,
    rx_buf: Buffer,
    next_unique: u32,
    in_flight: HashMap<u32, oneshot::Sender<PyResult<Packet>>>,
}

impl PortMaster {
    async fn drive(port_init: PortInit) {
        let port = port_init.port;
        let (rx, tx) = tokio::io::split(port);
        let (rx, tx) = (BufReader::new(rx), BufWriter::new(tx));

        let (cmd_tx, cmd_rx) = mpsc::channel(1);
        let rx_buf = BufferPool::take(Default::default());

        let mut master = PortMaster {
            rx,
            tx,
            cmd_rx,
            rx_buf,
            next_unique: 0,
            in_flight: Default::default(),
        };

        let result = master.handshake(port_init.crc32).await.map(|()| cmd_tx);
        let _ = port_init.tx.send(result);

        master.rx_buf.as_mut().clear();
        master.run().await;
    }

    async fn run(&mut self) {
        loop {
            tokio::select! {
                biased;

                cmd = self.cmd_rx.recv() => {
                    match cmd {
                        None => break,
                        Some(cmd) => self.handle_cmd(cmd).await,
                    }
                }

                result = read_packet(&mut self.rx, self.rx_buf.as_mut()) => {
                    match result {
                        Err(_) => todo!(),
                        Ok(()) => {
                            self.handle_incoming();
                            self.rx_buf.as_mut().clear();
                        }
                    }
                }
            }
        }
    }

    fn handle_incoming(&mut self) {
        let buf = self.rx_buf.as_ref();
        let (header, body) = match take_from_bytes::<Msg<_>>(buf) {
            Err(_) => todo!(),
            Ok(taken) => taken,
        };

        let body_offset = buf.len() - body.len();
        let tx = match self.in_flight.remove(&header.unique) {
            None => todo!(),
            Some(tx) => tx,
        };

        let next_rx_buf = self.take_buf();
        let packet = Packet {
            buf: std::mem::replace(&mut self.rx_buf, next_rx_buf),
            header,
            body_offset,
        };

        let _ = tx.send(Ok(packet));
    }

    async fn handle_cmd(&mut self, cmd: RequestCmd) {
        let mut buf = self.take_buf();

        match cmd {
            RequestCmd::New { tx } => {
                let unique = self.next_unique;
                self.next_unique += 1;

                let header = Msg {
                    unique,
                    flags: 0,
                    errno: 0,
                    body: (),
                };

                let result = append_raw(&header, buf.as_mut())
                    .map_err(postcard_err)
                    .map(|()| Packet {
                        body_offset: buf.as_ref().len(),
                        buf,
                        header,
                    });

                let _ = tx.send(result);
            }

            RequestCmd::Send { request, tx } => {
                let source = request.buf.as_ref();
                let encoded = buf.as_mut();
                encoded.resize(max_encoding_length(source.len()) + 1, b'\0');

                let length = encode(source, encoded) + 1;

                let mut result = self.tx.write_all(&encoded[..length]).await;
                if let Ok(()) = result {
                    result = self.tx.flush().await;
                }

                match result {
                    Ok(()) => {
                        self.in_flight.insert(request.header.unique, tx);
                    }

                    Err(err) => {
                        let _ = tx.send(Err(err.into()));
                    }
                }
            }
        }
    }

    async fn handshake(&mut self, crc32: u32) -> PyResult<()> {
        let proto = proto::ProtoVersion::BUILT;
        let buf = self.rx_buf.as_mut();

        buf.clear();
        append_cobs(&proto, buf).map_err(postcard_err)?;

        self.tx.write_all(b"\0\0").await?;
        self.tx.write_all(buf).await?;
        self.tx.flush().await?;

        buf.clear();
        read_packet(&mut self.rx, buf).await?;
        let dev_proto = from_bytes::<proto::ProtoVersion>(buf).map_err(postcard_err)?;

        if dev_proto != proto {
            let err = format!("Version mismatch: host has v{proto}, dev has v{dev_proto}");
            return Err(ProtocolError::new_err(err));
        }

        buf.clear();
        read_packet(&mut self.rx, buf).await?;

        let init = from_bytes::<proto::DeviceInit>(buf).map_err(postcard_err)?;
        if init.crc32 != crc32 {
            let err = format!(
                "CRC 0x{:08x} != 0x{:08x}: device implements another schema",
                init.crc32, crc32
            );

            return Err(ProtocolError::new_err(err));
        }

        Ok(())
    }

    fn take_buf(&self) -> Buffer {
        BufferPool::take(Arc::clone(&self.rx_buf.pool))
    }
}

struct Buffer {
    buf: Vec<u8>,
    pool: Arc<Mutex<BufferPool>>,
}

impl AsRef<Vec<u8>> for Buffer {
    fn as_ref(&self) -> &Vec<u8> {
        &self.buf
    }
}

impl AsMut<Vec<u8>> for Buffer {
    fn as_mut(&mut self) -> &mut Vec<u8> {
        &mut self.buf
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        let buf = std::mem::take(&mut self.buf);
        if buf.capacity() > 0 {
            if let Ok(mut pool) = self.pool.lock() {
                pool.0.push(buf);
            }
        }
    }
}

#[derive(Default)]
struct BufferPool(Vec<Vec<u8>>);

impl BufferPool {
    fn take(pool: Arc<Mutex<Self>>) -> Buffer {
        const BUF_CAPACITY: usize = 4096;

        let buf = {
            let mut pool = pool.lock().unwrap();
            if let Some(mut buf) = pool.0.pop() {
                buf.clear();
                buf
            } else {
                Vec::with_capacity(BUF_CAPACITY)
            }
        };

        Buffer { buf, pool }
    }
}

fn deserialize_typed(py: Python, packet: &Packet, ty: &Arc<Type>) -> PyResult<PyObject> {
    #[derive(Copy, Clone)]
    struct Typed<'a> {
        ty: &'a Arc<Type>,
    }

    impl<'de> DeserializeSeed<'de> for Typed<'_> {
        type Value = Dyn;

        fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: de::Deserializer<'de>,
        {
            use Type::*;

            match &**self.ty {
                Bool => deserializer.deserialize_bool(self),
                U8 => deserializer.deserialize_u8(self),
                U16 => deserializer.deserialize_u16(self),
                U32 => deserializer.deserialize_u32(self),
                I32 => deserializer.deserialize_i32(self),
                Tuple(items) => deserializer.deserialize_tuple(items.len(), self),
                Struct { fields, .. } => deserializer.deserialize_tuple(fields.len(), self),
                Array(_of, length) => deserializer.deserialize_tuple(*length as usize, self),
                Option(_of) => deserializer.deserialize_option(self),
            }
        }
    }

    impl<'de> de::Visitor<'de> for Typed<'_> {
        type Value = Dyn;

        fn expecting(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
            write!(fmt, "{}", self.ty)
        }

        fn visit_bool<E>(self, value: bool) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(Dyn::Bool(value))
        }

        fn visit_u8<E>(self, value: u8) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(Dyn::U8(value))
        }

        fn visit_u16<E>(self, value: u16) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(Dyn::U16(value))
        }

        fn visit_u32<E>(self, value: u32) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(Dyn::U32(value))
        }

        fn visit_i32<E>(self, value: i32) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(Dyn::I32(value))
        }

        fn visit_none<E>(self) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(Dyn::Option(None))
        }

        fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: de::Deserializer<'de>,
        {
            let ty = match &**self.ty {
                Type::Option(ty) => Typed { ty },
                _ => unreachable!(),
            };

            ty.deserialize(deserializer)
                .map(|value| Dyn::Option(Some(Arc::new(value))))
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: de::SeqAccess<'de>,
        {
            let mut next = |ty| seq.next_element_seed(Typed { ty }).transpose();

            let items = match &**self.ty {
                Type::Tuple(items) => items
                    .iter()
                    .filter_map(|ty| next(ty))
                    .collect::<Result<_, _>>()?,

                Type::Struct { fields, .. } => fields
                    .iter()
                    .filter_map(|(_name, ty)| next(ty))
                    .collect::<Result<_, _>>()?,

                Type::Array(of, length) => std::iter::from_fn(|| next(of))
                    .take(*length as usize)
                    .collect::<Result<_, _>>()?,

                _ => unreachable!(),
            };

            Ok(Dyn::Product(items))
        }
    }

    let body_seed = Typed { ty };
    let mut de = postcard::Deserializer::from_bytes(packet.body());

    body_seed
        .deserialize(&mut de)
        .map(|value| DynOf(value, ty).into_py(py))
        .map_err(postcard_err)
}

async fn read_packet<R>(port: &mut R, buf: &mut Vec<u8>) -> PyResult<()>
where
    R: Unpin + AsyncBufRead,
{
    port.take((buf.capacity() - buf.len()) as _)
        .read_until(0u8, buf)
        .await?;

    let length =
        decode_in_place(buf).map_err(|()| postcard_err(postcard::Error::DeserializeBadEncoding))?;

    buf.resize_with(length, || unreachable!());
    Ok(())
}

fn append_raw<T: Serialize>(ser: &T, buf: &mut Vec<u8>) -> postcard::Result<()> {
    append(ser, buf, AppendMode::Raw)
}

fn append_cobs<T: Serialize>(ser: &T, buf: &mut Vec<u8>) -> postcard::Result<()> {
    append(ser, buf, AppendMode::Cobs)
}

enum AppendMode {
    Raw,
    Cobs,
}

fn append<T>(ser: &T, buf: &mut Vec<u8>, mode: AppendMode) -> postcard::Result<()>
where
    T: Serialize,
{
    let append = Append {
        offset: buf.len(),
        buf,
    };

    match mode {
        AppendMode::Raw => serialize_with_flavor(ser, append),
        AppendMode::Cobs => {
            let flavor = postcard::flavors::Cobs::try_new(append).unwrap();
            serialize_with_flavor(ser, flavor)
        }
    }
}

struct Append<'a> {
    buf: &'a mut Vec<u8>,
    offset: usize,
}

impl Index<usize> for Append<'_> {
    type Output = u8;

    fn index(&self, index: usize) -> &Self::Output {
        self.buf.as_slice().index(self.offset + index)
    }
}

impl IndexMut<usize> for Append<'_> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.buf.as_mut_slice().index_mut(self.offset + index)
    }
}

impl SerFlavor for Append<'_> {
    type Output = ();

    fn try_push(&mut self, data: u8) -> Result<(), ()> {
        self.buf.push(data);
        Ok(())
    }

    fn release(self) -> Result<Self::Output, ()> {
        Ok(())
    }

    fn try_extend(&mut self, data: &[u8]) -> Result<(), ()> {
        self.buf.extend(data);
        Ok(())
    }
}

fn ok_or_broken<T, E>(result: Result<T, E>) -> PyResult<T> {
    result.map_err(|_| PyBrokenPipeError::new_err("Port master link severed"))
}

fn postcard_err(err: postcard::Error) -> PyErr {
    use postcard::Error::*;

    let message = match err {
        WontImplement => "A serialization feature won't be implemented",
        NotYetImplemented => "A serialization feature is not yet implemented",
        SerializeBufferFull => "The serialize buffer is full",
        SerializeSeqLengthUnknown => "The length of a sequence must be known",
        DeserializeUnexpectedEnd => "Hit the end of buffer, expected more data",
        DeserializeBadVarint => {
            "Found a variant that didn’t terminate. Is the usize too big for this platform?"
        }
        DeserializeBadBool => "Found a bool that wasn’t 0 or 1",
        DeserializeBadChar => "Found an invalid unicode char",
        DeserializeBadUtf8 => "Tried to parse invalid utf-8",
        DeserializeBadOption => "Found an Option discriminant that wasn’t 0 or 1",
        DeserializeBadEnum => "Found an enum discriminant that was > u32::max_value()",
        DeserializeBadEncoding => "The original data was not well encoded",
        SerdeSerCustom => "Unknown serialization error",
        SerdeDeCustom => "Unknown deserialization error",
    };

    ProtocolError::new_err(message)
}

impl Dyn {
    fn extract(arg: &PyAny, ty: &Type) -> PyResult<Self> {
        match ty {
            Type::Bool => arg.extract().map(Self::Bool),
            Type::U8 => arg.extract().map(Self::U8),
            Type::U16 => arg.extract().map(Self::U16),
            Type::U32 => arg.extract().map(Self::U32),
            Type::I32 => arg.extract().map(Self::I32),

            Type::Tuple(items) => {
                let tuple: &PyTuple = arg.extract()?;

                let (len, expected) = (tuple.len(), items.len());
                if len != expected {
                    let err = format!("Expected {expected}-tuple, found {len}-tuple");
                    return Err(PyTypeError::new_err(err));
                }

                items
                    .iter()
                    .zip(tuple.iter())
                    .map(|(ty, value)| Self::extract(value, ty))
                    .collect::<Result<_, _>>()
                    .map(Self::Product)
            }

            Type::Array(of, length) => {
                let list: &PyList = arg.extract()?;
                let length = *length as usize;

                if list.len() != length {
                    let err = format!("Expected length {length}, found length {}", list.len());
                    return Err(PyValueError::new_err(err));
                }

                list.iter()
                    .map(|element| Self::extract(element, of))
                    .collect::<Result<_, _>>()
                    .map(Self::Product)
            }

            Type::Option(_of) if arg.is_none() => Ok(Self::Option(None)),
            Type::Option(of) => {
                Self::extract(arg, of).map(|value| Self::Option(Some(Arc::new(value))))
            }

            _ => {
                let synthetic: &PyCell<Synthetic> = arg.extract()?;
                let synthetic = synthetic.try_borrow()?;

                let other_ty = &*synthetic.ty;
                if !std::ptr::eq(ty, other_ty) {
                    let err = format!("Expected '{ty}', got '{other_ty}'");
                    return Err(PyTypeError::new_err(err));
                }

                Ok(synthetic.value.clone())
            }
        }
    }
}

struct DynOf<'a>(Dyn, &'a Arc<Type>);

impl IntoPy<PyObject> for DynOf<'_> {
    fn into_py(self, py: Python) -> PyObject {
        let (value, ty) = (self.0, self.1);
        match (value, &**ty) {
            (Dyn::Bool(value), _) => value.into_py(py),
            (Dyn::U8(value), _) => value.into_py(py),
            (Dyn::U16(value), _) => value.into_py(py),
            (Dyn::U32(value), _) => value.into_py(py),
            (Dyn::I32(value), _) => value.into_py(py),

            (Dyn::Product(values), Type::Tuple(tys)) => {
                let pairs = values
                    .into_iter()
                    .cloned()
                    .zip(tys.iter())
                    .map(|(value, ty)| DynOf(value, ty).into_py(py));

                PyTuple::new(py, pairs).into_py(py)
            }

            (Dyn::Product(values), Type::Array(of, _length)) => {
                let values = values
                    .into_iter()
                    .cloned()
                    .map(|value| DynOf(value, of).into_py(py));

                PyList::new(py, values).into_py(py)
            }

            (Dyn::Option(None), Type::Option(_of)) => py.None(),
            (Dyn::Option(Some(value)), Type::Option(of)) => {
                DynOf(Dyn::clone(&value), of).into_py(py)
            }

            (value, _) => {
                let synthetic = Synthetic {
                    ty: Arc::clone(ty),
                    value,
                };

                Py::new(py, synthetic)
                    .expect("Py::new() should never fail")
                    .into_py(py)
            }
        }
    }
}

#[derive(Copy, Clone)]
struct DynRepr<'a>(&'a Dyn, &'a Type);

impl Display for DynRepr<'_> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match (self.0, self.1) {
            (Dyn::Bool(value), _) => write!(fmt, "{}", value),
            (Dyn::U8(value), _) => write!(fmt, "{}", value),
            (Dyn::U16(value), _) => write!(fmt, "{}", value),
            (Dyn::U32(value), _) => write!(fmt, "{}", value),
            (Dyn::I32(value), _) => write!(fmt, "{}", value),

            (Dyn::Product(items), Type::Tuple(tys)) => {
                fmt.write_char('(')?;

                let mut items = items
                    .iter()
                    .zip(tys.iter())
                    .map(|(item, ty)| DynRepr(item, ty));

                if let Some(first) = items.next() {
                    write!(fmt, "{first}")?;
                }

                for item in items {
                    write!(fmt, ", {item}")?;
                }

                fmt.write_char(')')
            }

            (Dyn::Product(items), Type::Array(of, _length)) => {
                fmt.write_char('[')?;

                let mut items = items.iter().map(|item| DynRepr(item, of));
                if let Some(first) = items.next() {
                    write!(fmt, "{first}")?;
                }

                for item in items {
                    write!(fmt, ", {item}")?;
                }

                fmt.write_char(']')
            }

            (Dyn::Option(None), Type::Option(_of)) => fmt.write_str("None"),
            (Dyn::Option(Some(value)), Type::Option(of)) => DynRepr(value, of).fmt(fmt),

            (Dyn::Product(items), Type::Struct { name, fields }) => {
                write!(fmt, "{name}(")?;

                let mut first = true;
                for (field, value) in fields.iter().zip(items.iter()) {
                    if !first {
                        fmt.write_str(", ")?;
                    }

                    let (name, ty) = field;
                    write!(fmt, "{name}={}", DynRepr(value, ty))?;

                    first = false;
                }

                fmt.write_char(')')
            }

            _ => Err(fmt::Error),
        }
    }
}

impl Serialize for Dyn {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Dyn::Bool(value) => serializer.serialize_bool(*value),
            Dyn::U8(value) => serializer.serialize_u8(*value),
            Dyn::U16(value) => serializer.serialize_u16(*value),
            Dyn::U32(value) => serializer.serialize_u32(*value),
            Dyn::I32(value) => serializer.serialize_i32(*value),

            Dyn::Product(items) => {
                let mut ser = serializer.serialize_tuple(items.len())?;
                items
                    .iter()
                    .try_for_each(|value| ser.serialize_element(value))?;

                ser.end()
            }

            Dyn::Option(value) => match value {
                None => serializer.serialize_none(),
                Some(value) => serializer.serialize_some(&**value),
            },
        }
    }
}
