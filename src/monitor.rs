use crate::frames::{CobsBuf, FrameFlavor, FrameWrite};
use crate::{actor, ControlDevice, DeviceMask, Queues};
use control_proto::{Msg, ProtoVersion};
use core::marker::PhantomData;
use nb::Error::WouldBlock;
use postcard::{serialize_with_flavor, take_from_bytes_cobs};
use serde::{de::DeserializeOwned, Serialize};

pub struct MonitorTx {
    buf: [u8; 64],
    start: usize,
    end: usize,
    write_locked: bool,
}

pub struct Monitor<Dev> {
    rx_state: RxState,
    rx_buf: heapless::Vec<u8, 64>,
    crc32: u32,
    _phantom: PhantomData<Dev>,
}

impl MonitorTx {
    pub fn available(&self) -> &[u8] {
        &self.buf[self.start..self.end]
    }

    pub fn consume(&mut self, count: usize) {
        self.start += (self.end - self.start).min(count);
        if self.start == self.end {
            self.start = 0;
            self.end = 0;
        }
    }

    pub async fn __transmit_msg(queues: &impl Queues, data: &Msg<impl Serialize>) {
        transmit(queues, data).await
    }
}

impl FrameWrite for MonitorTx {
    fn write(&mut self, data: &[u8]) -> usize {
        if self.end >= self.buf.len() {
            0
        } else {
            let written = data.len().min(self.buf.len() - self.end);
            let (start, end) = (self.end, self.end + written);

            (&mut self.buf[start..end]).copy_from_slice(&data[..written]);
            self.end = end;

            written
        }
    }
}

impl Default for MonitorTx {
    fn default() -> Self {
        MonitorTx {
            buf: [0; 64],
            start: 0,
            end: 0,
            write_locked: false,
        }
    }
}

impl<Dev> Default for Monitor<Dev> {
    fn default() -> Self {
        Monitor {
            rx_state: RxState::InitDrop,
            rx_buf: Default::default(),
            crc32: Default::default(),
            _phantom: PhantomData,
        }
    }
}

impl<Dev: ControlDevice> Monitor<Dev> {
    pub fn __set_schema_crc32(&mut self, crc32: u32) {
        self.crc32 = crc32;
    }

    fn receive(&mut self, queues: &Dev::Queues<'_>) -> Result<(), ()> {
        self.take()
            .and_then(|msg| queues.try_post(msg).map_err(drop))
    }

    async fn handshake(&mut self, queues: &Dev::Queues<'_>) -> Result<(), ()> {
        let version: ProtoVersion = self.take()?;

        let built = ProtoVersion::BUILT;
        transmit(queues, &built).await;

        (version == built).then(|| ()).ok_or(())?;
        transmit(queues, &self.crc32.to_le_bytes()).await;

        self.rx_state = RxState::ReadyFill;
        Ok(())
    }

    fn take<T: DeserializeOwned>(&mut self) -> Result<T, ()> {
        match take_from_bytes_cobs(&mut self.rx_buf) {
            Ok((ok, _trailing)) => Ok(ok),
            Err(_) => Err(()),
        }
    }
}

#[actor]
impl<Dev: ControlDevice> Monitor<Dev> {
    pub async fn feed(&mut self, queues: &Dev::Queues<'_>, data: heapless::Vec<u8, 64>) {
        for byte in data.into_iter() {
            use RxState::*;
            let result = match (self.rx_state, byte) {
                (InitDrop, b'\0') => {
                    self.rx_state = RxState::InitFill;
                    Ok(())
                }

                (ReadyDrop, b'\0') => {
                    self.rx_state = RxState::ReadyFill;
                    Ok(())
                }

                (InitDrop | ReadyDrop, _) => Ok(()),

                (InitFill, b'\0') => self.handshake(queues).await,
                (ReadyFill, b'\0') => self.receive(queues),
                (InitFill | ReadyFill, byte) => self.rx_buf.push(byte).map_err(drop),
            };

            if byte == b'\0' {
                if self.rx_buf.is_empty() {
                    self.rx_state = InitFill;
                } else {
                    self.rx_buf.clear();
                }
            }

            if let Err(()) = result {
                //TODO: report error to host
                self.rx_state = match self.rx_state {
                    state if byte == b'\0' => state,
                    InitDrop | InitFill => InitDrop,
                    ReadyDrop | ReadyFill => ReadyDrop,
                };
            }
        }
    }
}

#[derive(Copy, Clone)]
enum RxState {
    InitDrop,
    InitFill,
    ReadyDrop,
    ReadyFill,
}

async fn transmit<Q: Queues>(queues: &Q, data: &impl Serialize) {
    let mut locked = false;
    let mut cobs = CobsBuf::default();

    let ev = <Q::Ev as DeviceMask>::monitor_tx();
    let out = queues.signals().drive_infallible(ev, || {
        let mut tx = queues.monitor_tx()?;
        if !tx.write_locked {
            tx.write_locked = true;
            locked = true;
        } else if !locked {
            return Err(WouldBlock);
        }

        let last_available = tx.available().len();

        let result = serialize_with_flavor(data, FrameFlavor::new(&mut *tx, &mut cobs));
        if tx.available().len() > last_available {
            queues.signals().raise(ev);
        }

        //TODO: handle errors other than SerializeBufferFull
        result.map_err(|_| WouldBlock)?;

        tx.write_locked = false;
        Ok(())
    });

    out.await;
}
