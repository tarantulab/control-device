#![no_std]
#![feature(generic_associated_types)]

use control_proto::Msg;
use core::{cell::RefMut, convert::Infallible};
use nb_executor::{EventMask, Mpmc, Signals};
use serde::de::DeserializeOwned;

pub use control_device_proc::{actor, ControlDevice, Rpc};
pub use control_proto as proto;

pub use futures;
pub use nb;
pub use nb_executor;

mod frames;
mod monitor;

pub use monitor::{Monitor, MonitorTx};

pub trait ControlDevice {
    type Ev: DeviceMask;
    type Queues<'a>: Queues<Ev = Self::Ev>;

    fn queues<'a>(signals: &'a Signals<'a, Self::Ev>, ev: Self::Ev) -> Self::Queues<'a>;
}

pub trait DeviceMask: EventMask {
    fn monitor_tx() -> Self;
}

pub trait Actor {
    type Body: DeserializeOwned;
    type Senders<'a, Ev: DeviceMask, const N: usize>;

    fn senders<'a, Ev: DeviceMask, const N: usize>(
        queue: &'a Mpmc<Msg<Self::Body>, N>,
        signals: &'a Signals<'a, Ev>,
        ev: Ev,
    ) -> Self::Senders<'a, Ev, N>;
}

pub trait Queues {
    type Ev: DeviceMask;
    type Body: DeserializeOwned;

    fn ev(&self) -> Self::Ev;
    fn signals(&self) -> &Signals<Self::Ev>;
    fn try_post(&self, msg: Msg<Self::Body>) -> nb::Result<(), Infallible>;
    fn monitor_tx(&self) -> nb::Result<RefMut<MonitorTx>, Infallible>;
}
