use postcard::flavors::SerFlavor;
use postcard_cobs::{EncoderState, PushResult};

#[derive(Default)]
pub struct CobsBuf {
    retired: usize,
    state: EncoderState,
    leading_byte: u8,
    written: u8,
    buf: heapless::Vec<u8, 254>,
}

pub struct FrameFlavor<'a, W> {
    tx: &'a mut W,
    cobs: &'a mut CobsBuf,
    skip: usize,
}

pub trait FrameWrite {
    fn write(&mut self, data: &[u8]) -> usize;
}

impl<'a, W> FrameFlavor<'a, W> {
    pub fn new(tx: &'a mut W, cobs: &'a mut CobsBuf) -> Self {
        let skip = cobs.retired;
        FrameFlavor { tx, cobs, skip }
    }
}

impl<W: FrameWrite> FrameFlavor<'_, W> {
    fn flush(&mut self) -> Result<(), ()> {
        if self.cobs.leading_byte == b'\0' {
            return Ok(());
        } else if self.cobs.written == 0 {
            let leading = [self.cobs.leading_byte];
            (self.tx.write(&leading) == 1).then(|| ()).ok_or(())?;

            self.cobs.written += 1;
        }

        let skip = (self.cobs.written - 1).into();
        self.cobs.written += self.tx.write(&self.cobs.buf[skip..]) as u8;

        let end = (1 + self.cobs.buf.len()) as u8;
        (self.cobs.written == end).then(|| ()).ok_or(())?;

        self.cobs.leading_byte = b'\0';
        self.cobs.written = 0;
        self.cobs.buf.clear();

        Ok(())
    }
}

impl<W: FrameWrite> SerFlavor for FrameFlavor<'_, W> {
    type Output = ();

    fn try_push(&mut self, byte: u8) -> Result<(), ()> {
        if self.skip > 0 {
            self.skip -= 1;
            return Ok(());
        }

        self.flush()?;
        self.cobs.retired += 1;

        match self.cobs.state.push(byte) {
            PushResult::AddSingle(byte) => {
                self.cobs.buf.push(byte).unwrap();
            }

            PushResult::ModifyFromStartAndSkip((_idx, leading)) => {
                self.cobs.leading_byte = leading;
            }

            PushResult::ModifyFromStartAndPushAndSkip((_idx, leading, next)) => {
                self.cobs.leading_byte = leading;
                self.cobs.buf.push(next).unwrap();
            }
        }

        Ok(())
    }

    fn release(mut self) -> Result<Self::Output, ()> {
        self.flush()?;

        let (_idx, leading) = self.cobs.state.clone().finalize();
        self.cobs.leading_byte = leading;

        match self.cobs.buf.last() {
            Some(b'\0') => (),
            _ => self.cobs.buf.push(b'\0').unwrap(),
        }

        self.flush()
    }
}
