use std::{env, fs, path::PathBuf};

fn main() {
    let out: PathBuf = env::var("OUT_DIR").unwrap().into();
    fs::copy("control_device.x", out.join("control_device.x")).unwrap();

    println!("cargo:rustc-link-search={}", out.display());
}
