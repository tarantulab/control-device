#![feature(drain_filter)]

use proc_macro::TokenStream;
use proc_macro2::{Span, TokenStream as TokenStream2};
use quote::quote;
use syn::{fold::Fold, punctuated::Punctuated, spanned::Spanned, *};

#[proc_macro_derive(ControlDevice, attributes(events))]
pub fn derive_control_device(item: TokenStream) -> TokenStream {
    device_struct(parse_macro_input!(item as DeriveInput))
        .unwrap_or_else(Error::into_compile_error)
        .into()
}

fn device_struct(mut input: DeriveInput) -> Result<TokenStream2> {
    let ev_ty: Type = pop_attr(&mut input.attrs, "events")
        .ok_or_else(|| Error::new_spanned(&input.ident, "missing `#[events]`"))
        .and_then(|attr| attr.parse_args())?;

    let ident = &input.ident;
    let fields = match &input.data {
        Data::Struct(DataStruct {
            fields: Fields::Named(fields),
            ..
        }) => fields
            .named
            .iter()
            .map(|field| (field.ident.as_ref().unwrap(), &field.ty)),

        _ => {
            let err = "expected record-like device struct";
            return Err(Error::new_spanned(input, err));
        }
    };

    for param in input.generics.params.iter() {
        use GenericParam::*;

        let ident = match param {
            Type(param) => &param.ident,
            Const(param) => &param.ident,
            Lifetime(_) => continue,
        };

        let err = "The device struct must not be generic over non-lifetime parameters";
        return Err(Error::new_spanned(ident, err));
    }

    if !fields.clone().any(|(ident, _ty)| ident == "monitor") {
        return Err(Error::new_spanned(ident, "Missing `monitor` actor"));
    }

    let without_lifetimes = fields
        .clone()
        .map(|(ident, ty)| {
            struct FoldLifetimes;

            impl Fold for FoldLifetimes {
                fn fold_path_arguments(&mut self, args: PathArguments) -> PathArguments {
                    use PathArguments::*;

                    match args {
                        None => None,

                        Parenthesized(args) => {
                            let args = self.fold_parenthesized_generic_arguments(args);
                            Parenthesized(args)
                        }

                        AngleBracketed(args) => {
                            let punctuated = args
                                .args
                                .into_pairs()
                                .filter(|arg| {
                                    use GenericArgument::Lifetime;

                                    match arg.value() {
                                        Lifetime(lt) if lt.ident != "static" => false,
                                        _ => true,
                                    }
                                })
                                .collect();

                            let args = AngleBracketedGenericArguments {
                                args: punctuated,
                                ..args
                            };

                            let args = self.fold_angle_bracketed_generic_arguments(args);
                            AngleBracketed(args)
                        }
                    }
                }
            }

            (ident, FoldLifetimes.fold_type(ty.clone()))
        })
        .collect::<Vec<_>>();

    let schema_tys = without_lifetimes.iter().map(|(_ident, ty)| {
        quote! {
            let __schema = <#ty>::__put_types(__schema);
        }
    });

    let schema_modules = without_lifetimes.iter().map(|(ident, ty)| {
        let name = ident_to_string(ident);

        quote! {
            let __schema = <#ty>::__write_module(__schema.module(#name));
        }
    });

    let queues = fields.clone().map(|(ident, ty)| {
        quote! {
            #ident: __Mpmc<__proto::Msg<<#ty as __Actor>::Body>, 2>
        }
    });

    let futures = fields.clone().map(|(ident, ty)| {
        quote! {
            <#ty>::__actor_loop(&mut self.#ident, queues, &queues.#ident)
        }
    });

    let queue_inits = fields.clone().map(|(ident, _ty)| {
        quote! {
            #ident: __Mpmc::default()
        }
    });

    let post_arms = fields.clone().map(|(ident, _ty)| {
        quote! {
            __DevMsg::#ident(body) => {
                let msg = header.attach(body);
                self.#ident.try_enqueue(msg, signals, ev).map_err(|_| __WouldBlock)
            }
        }
    });

    let senders = fields.clone().map(|(ident, ty)| {
        quote! {
            pub fn #ident(&self) -> <#ty as __Actor>::Senders<'_, #ev_ty, 2> {
                <#ty as __Actor>::senders(&self.#ident, self.__signals, self.__ev)
            }
        }
    });

    let variants = fields.map(|(ident, ty)| {
        let bound = quote!(<#ty as __Actor>::Body: __serde::Deserialize<'de>).to_string();
        let bound = LitStr::new(&bound, ty.span());

        quote! {
            #[serde(bound(deserialize = #bound))]
            #ident(<#ty as __Actor>::Body)
        }
    });

    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

    let mut queue_generics = input.generics.clone();
    let (queue_impl_generics, queue_ty_generics, _) = {
        queue_generics.lt_token.get_or_insert_with(Default::default);
        queue_generics.gt_token.get_or_insert_with(Default::default);

        let lifetime = LifetimeDef {
            attrs: Vec::new(),
            lifetime: Lifetime {
                apostrophe: Span::call_site(),
                ident: Ident::new("__s", Span::call_site()),
            },
            colon_token: None,
            bounds: Punctuated::new(),
        };

        let lifetime = GenericParam::Lifetime(lifetime);

        queue_generics.params.insert(0, lifetime);
        queue_generics.split_for_impl()
    };

    let crate_path = control_device_crate();
    let serde_attrs = serde_attrs();

    let expanded = quote! {
        const _: () = {
            use #crate_path::{
                futures as __futures,
                nb_executor::{Mpmc as __Mpmc, Signals as __Signals},
                nb::{self as __nb, Error::WouldBlock as __WouldBlock},
                proto as __proto,
                Actor as __Actor,
                ControlDevice as __ControlDevice,
                MonitorTx as __MonitorTx,
                Queues as __QueuesTrait,
            };

            use core::{
                cell::{RefCell as __RefCell, RefMut as __RefMut},
                convert::Infallible as __Infallible
            };

            extern crate serde as __serde;

            #serde_attrs
            pub enum __DevMsg #impl_generics
                #where_clause
            {
                #(#variants),*
            }

            pub struct __Queues #queue_impl_generics
                #where_clause
            {
                __signals: &'__s __Signals<'__s, #ev_ty>,
                __ev: #ev_ty,
                __monitor_tx: __RefCell<__MonitorTx>,
                #(#queues),*
            }

            impl #queue_impl_generics __Queues #queue_ty_generics
                #where_clause
            {
                #(#senders)*
            }

            impl #queue_impl_generics __QueuesTrait for __Queues #queue_ty_generics
                #where_clause
            {
                type Ev = #ev_ty;
                type Body = __DevMsg #ty_generics;

                fn ev(&self) -> Self::Ev {
                    self.__ev
                }

                fn signals(&self) -> &__Signals<Self::Ev> {
                    self.__signals
                }

                fn try_post(&self, msg: __proto::Msg<Self::Body>) -> __nb::Result<(), __Infallible> {
                    let (header, body) = msg.split();
                    let signals = self.__signals;
                    let ev = self.__ev;

                    match body {
                        #(#post_arms),*
                    }
                }

                fn monitor_tx(&self) -> __nb::Result<__RefMut<__MonitorTx>, __Infallible> {
                    self.__monitor_tx.try_borrow_mut().map_err(|_| __nb::Error::WouldBlock)
                }
            }

            impl #impl_generics __ControlDevice for #ident #ty_generics
                #where_clause
            {
                type Ev = #ev_ty;
                type Queues<'__s> = __Queues #queue_ty_generics;

                fn queues<'__a>(
                    signals: &'__a __Signals<'__a, Self::Ev>,
                    ev: Self::Ev
                ) -> Self::Queues<'__a> {
                    __Queues {
                        __signals: signals,
                        __ev: ev,
                        __monitor_tx: Default::default(),
                        #(#queue_inits),*
                    }
                }
            }

            impl #impl_generics #ident #ty_generics
                #where_clause
            {
                async fn run(&mut self, queues: &<Self as __ControlDevice>::Queues<'_>) -> ! {
                    const __SCHEMA: __proto::Schema = {
                        let __schema = __proto::Schema::new();
                        #(#schema_tys)*
                        #(#schema_modules)*
                        __schema
                    };

                    #[used]
                    #[no_mangle]
                    #[export_name = "__control_schema"]
                    #[link_section = ".control_device"]
                    static __EXPORT_ROOT: __proto::Schema = __SCHEMA;

                    const __CRC32: u32 = __SCHEMA.crc32();
                    self.monitor.__set_schema_crc32(__CRC32);

                    #[allow(unreachable_code)]
                    {
                        __futures::join!(#(#futures),*);
                        ::core::unreachable!()
                    }
                }
            }
        };
    };

    Ok(expanded)
}

#[proc_macro_derive(Rpc)]
pub fn derive_rpc(item: TokenStream) -> TokenStream {
    rpc(parse_macro_input!(item as DeriveInput))
        .unwrap_or_else(Error::into_compile_error)
        .into()
}

fn rpc(input: DeriveInput) -> Result<TokenStream2> {
    let ident = &input.ident;
    let name = ident_to_string(ident);

    let ty_desc = match &input.data {
        Data::Struct(DataStruct {
            fields: Fields::Named(fields),
            ..
        }) => {
            let fields = fields.named.iter().map(|field| {
                let name = ident_to_string(field.ident.as_ref().unwrap());
                let ty = &field.ty;

                quote!((#name, <#ty as __Rpc>::TY))
            });

            quote! {
                __Type::Struct {
                    name: #name,
                    fields: &[ #(#fields),* ],
                }
            }
        }

        _ => {
            let err = "expected record-likes struct";
            return Err(Error::new_spanned(input, err));
        }
    };

    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let crate_path = control_device_crate();

    let expanded = quote! {
        const _: () = {
            use #crate_path::proto::{Rpc as __Rpc, Type as __Type};

            impl #impl_generics __Rpc for #ident #ty_generics
                #where_clause
            {
                const TY: __Type = #ty_desc;
            }
        };
    };

    Ok(expanded)
}

#[proc_macro_attribute]
pub fn actor(_attr: TokenStream, item: TokenStream) -> TokenStream {
    actor_impl(parse_macro_input!(item as ItemImpl))
        .unwrap_or_else(Error::into_compile_error)
        .into()
}

fn actor_impl(mut input: ItemImpl) -> Result<TokenStream2> {
    if input.trait_.is_some() {
        let err = "actor impl must be an inherent impl, not trait impl";
        return Err(Error::new_spanned(input, err));
    }

    struct Param<'a> {
        name: &'a Ident,
        ty: &'a Type,
    }

    struct Method<'a> {
        name: &'a Ident,
        export: bool,
        fields: Vec<Param<'a>>,
        returns: &'a ReturnType,
    }

    let mut methods = Vec::new();
    let mut queues_ty = None;

    for item in &mut input.items {
        let method = match item {
            ImplItem::Method(method) => method,
            _ => continue,
        };

        let sig = &method.sig;
        if sig.asyncness.is_none() {
            return Err(Error::new_spanned(method, "actor methods must be async"));
        }

        let export = pop_attr(&mut method.attrs, "rpc").is_some();
        if let (false, ReturnType::Type(_, ty)) = (export, &sig.output) {
            let err = "internal actor methods cannot produce an output";
            return Err(Error::new_spanned(ty, err));
        }

        let mut params = sig.inputs.iter();

        match params.next() {
            Some(FnArg::Receiver(recv)) if recv.reference.is_some() => (),

            _ => {
                let err = "not an actor method (must take `self` by reference)";
                return Err(Error::new_spanned(&sig.ident, err));
            }
        }

        match params.next() {
            Some(FnArg::Typed(param)) => {
                let ident = expect_pat_ident(&param.pat)?;
                if ident != "queues" && ident != "_queues" {
                    return Err(Error::new_spanned(ident, "expected `queues`"));
                }

                queues_ty.get_or_insert(param.ty.as_ref());
            }

            _ => {
                let err = "missing `queues` parameter (must be first after `self`)";
                return Err(Error::new_spanned(&sig.ident, err));
            }
        }

        let fields = params
            .map(|param| {
                let param = match param {
                    FnArg::Receiver(_) => unreachable!(),
                    FnArg::Typed(input) => input,
                };

                Ok(Param {
                    name: expect_pat_ident(&param.pat)?,
                    ty: &param.ty,
                })
            })
            .collect::<Result<Vec<_>>>()?;

        methods.push(Method {
            name: &sig.ident,
            export,
            fields,
            returns: &sig.output,
        });
    }

    let queues_ty = match queues_ty {
        Some(queues_ty) => queues_ty,
        None => {
            let err = "no actor methods were defined, cannot infer queue set type";
            return Err(Error::new_spanned(&input, err));
        }
    };

    // Maintain index continuity of exported methods by relocating internal methods.
    // This should be a stable sort.
    methods.sort_by(|a, b| b.export.cmp(&a.export));

    let variants = methods.iter().map(|method| {
        let ident = method.name;
        let fields = method.fields.iter().map(|field| field.name);
        let field_tys = method.fields.iter().map(|field| field.ty);

        let internal = (!method.export).then(|| {
            quote! {
                #[serde(skip_deserializing)]
            }
        });

        quote! {
            #internal
            #ident {
                #(#fields: #field_tys),*
            }
        }
    });

    let exported = methods
        .iter()
        .take_while(|method| method.export)
        .map(|method| {
            let returns = match method.returns {
                ReturnType::Default => quote!(()),
                ReturnType::Type(_arrow, ty) => quote!(#ty),
            };

            (method, returns)
        })
        .collect::<Vec<_>>();

    let put_types = exported.iter().map(|(method, returns)| {
        let param_tys = method.fields.iter().map(|param| param.ty);

        quote! {
            let __schema = __schema.put_type::<#returns>();
            #(let __schema = __schema.put_type::<#param_tys>();)*
        }
    });

    let write_module = exported.iter().map(|(method, returns)| {
        let name = ident_to_string(method.name);
        let params = method.fields.iter().map(|param| {
            let name = ident_to_string(param.name);
            let ty = param.ty;

            quote! {
                .param::<#ty>(#name)
            }
        });

        quote! {
            .cmd::<#returns>(#name)
            #(#params)*
            .finish()
        }
    });

    let senders = methods
        .iter()
        .skip_while(|method| method.export)
        .map(|method| {
            let fields = method.fields.iter().map(|param| param.name);
            let params = method.fields.iter().map(|param| {
                let ident = param.name;
                let ty = param.ty;

                quote!(#ident: #ty)
            });

            let ident = method.name;
            quote! {
                pub async fn #ident(self, #(#params),*) {
                    let __msg = __proto::Msg::zeroed(__ActorMsg::#ident { #(#fields),* });
                    self.__queue.enqueue(__msg, self.__signals, self.__ev).await;
                }
            }
        });

    let dispatch_arms = methods.iter().map(|method| {
        let ident = method.name;
        let fields = method.fields.iter().map(|field| field.name);
        let args = fields.clone();

        let reply = method.export.then(|| {
            quote! {
                __MonitorTx::__transmit_msg(__queues, &__header.attach(__out)).await;
            }
        });

        quote! {
            __ActorMsg::#ident { #(#fields),* } => {
                let __out = self.#ident(__queues, #(#args),*).await;
                #reply
            }
        }
    });

    let self_ty = input.self_ty.as_ref();
    let (impl_generics, _ty_generics, where_clause) = input.generics.split_for_impl();

    let crate_path = control_device_crate();
    let serde_attrs = serde_attrs();

    let inserted = quote! {
        const _: () = {
            use #crate_path::{
                nb_executor::{Mpmc as __Mpmc, Signals as __Signals},
                proto as __proto,
                Actor as __Actor,
                DeviceMask as __DeviceMask,
                MonitorTx as __MonitorTx,
                Queues as __Queues,
            };

            extern crate serde as __serde;

            #serde_attrs
            pub enum __ActorMsg {
                #(#variants),*
            }

            #[derive(Copy, Clone)]
            pub struct __Senders<'__a, __Ev: __DeviceMask, const __N: usize> {
                __queue: &'__a __Mpmc<__proto::Msg<__ActorMsg>, __N>,
                __signals: &'__a __Signals<'__a, __Ev>,
                __ev: __Ev,
            }

            impl<__Ev: __DeviceMask, const __N: usize> __Senders<'_, __Ev, __N> {
                #(#senders)*
            }

            impl #impl_generics __Actor for #self_ty
                #where_clause
            {
                type Body = __ActorMsg;
                type Senders<'__a, __Ev: __DeviceMask, const __N: usize>
                    = __Senders<'__a, __Ev, __N>;

                fn senders<'__a, __Ev: __DeviceMask, const __N: usize>(
                    __queue: &'__a __Mpmc<__proto::Msg<Self::Body>, __N>,
                    __signals: &'__a __Signals<'__a, __Ev>,
                    __ev: __Ev,
                ) -> Self::Senders<'__a, __Ev, __N> {
                    __Senders { __queue, __signals, __ev }
                }
            }

            impl #impl_generics #self_ty
                #where_clause
            {
                pub const fn __put_types(__schema: __proto::Schema) -> __proto::Schema {
                    #(#put_types)*
                    __schema
                }

                pub const fn __write_module(__module: __proto::WriteModule) -> __proto::Schema {
                    __module
                        #(#write_module)*
                        .finish()
                }

                pub async fn __actor_loop<const __N: usize>(
                    &mut self,
                    __queues: #queues_ty,
                    __rx: &__Mpmc<__proto::Msg<__ActorMsg>, __N>,
                ) -> ! {
                    loop {
                        let __signals = __Queues::signals(__queues);
                        let __ev = __Queues::ev(__queues);

                        let (__header, __body) = __rx.dequeue(__signals, __ev).await.split();
                        match __body {
                            #(#dispatch_arms),*
                        }
                    }
                }
            }
        };
    };

    // borrowck prevents us from putting `#input` in the previous `quote!`
    let expanded = quote! {
        #input
        #inserted
    };

    Ok(expanded)
}

fn ident_to_string(ident: &Ident) -> LitStr {
    LitStr::new(&ident.to_string(), ident.span())
}

fn expect_pat_ident(pat: &Pat) -> Result<&Ident> {
    match pat {
        Pat::Ident(pat) => Ok(&pat.ident),
        pat => Err(Error::new_spanned(pat, "expected ident pattern")),
    }
}

fn pop_attr(attrs: &mut Vec<Attribute>, ident: &str) -> Option<Attribute> {
    attrs.drain_filter(|attr| attr.path.is_ident(ident)).next()
}

fn serde_attrs() -> TokenStream2 {
    quote! {
        #[derive(__serde::Deserialize)]
        #[allow(non_camel_case_types)]
        #[serde(rename = "cmd")]
    }
}

fn control_device_crate() -> TokenStream2 {
    let found = proc_macro_crate::crate_name("control-device")
        .expect("`control-device` is present in `Cargo.toml`");

    use proc_macro_crate::FoundCrate::*;
    match found {
        Itself => quote!(crate),
        Name(name) => {
            let ident = Ident::new(&name, Span::call_site());
            quote!(::#ident)
        }
    }
}
