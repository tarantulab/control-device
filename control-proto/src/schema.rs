use konst::slice::{eq_bytes, slice_range, slice_up_to};

use super::ProtoVersion;
use crc::{Crc, CRC_32_CKSUM};
use impl_trait_for_tuples::impl_for_tuples;

pub trait Rpc {
    const TY: Type;
}

pub enum Type {
    Bool,
    U8,
    U16,
    U32,
    Tuple(&'static [Type]),
    Struct {
        name: &'static str,
        fields: &'static [(&'static str, Type)],
    },
    Array(&'static Type, u32),
    Option(&'static Type),
    I32,
}

#[repr(C)]
pub struct Schema {
    buf: [u8; MEM_MAX],
}

pub struct WriteModule {
    schema: Schema,
    cmds_len_off: u32,
    end: u32,
}

pub struct WriteCmd {
    module: WriteModule,
    params_len_off: u32,
}

impl Rpc for bool {
    const TY: Type = Type::Bool;
}

impl Rpc for u8 {
    const TY: Type = Type::U8;
}

impl Rpc for u16 {
    const TY: Type = Type::U16;
}

impl Rpc for u32 {
    const TY: Type = Type::U32;
}

impl Rpc for i32 {
    const TY: Type = Type::I32;
}

#[impl_for_tuples(5)]
impl Rpc for Tuple {
    const TY: Type = Type::Tuple(&[for_tuples!(#( Tuple::TY ),*)]);
}

impl<T: Rpc, const N: usize> Rpc for [T; N] {
    const TY: Type = Type::Array(&T::TY, N as u32);
}

impl<T: Rpc> Rpc for Option<T> {
    const TY: Type = Type::Option(&T::TY);
}

impl WriteModule {
    pub const fn finish(self) -> Schema {
        self.schema.write_u32(Schema::HDR_END, self.end)
    }

    pub const fn cmd<Ret: Rpc>(self, name: &str) -> WriteCmd {
        let cmds = self.schema.read_u32(self.cmds_len_off);
        let (schema, ret_off) = self
            .schema
            .write_u32(self.cmds_len_off, cmds + 1)
            .write_str(self.end, name);

        let ret_id = schema.type_id(&Ret::TY);
        let schema = schema.write_u32(ret_off, ret_id);
        let params_len_off = ret_off + 4;

        WriteCmd {
            params_len_off,
            module: WriteModule {
                schema,
                end: params_len_off + 4,
                ..self
            },
        }
    }
}

impl WriteCmd {
    pub const fn finish(self) -> WriteModule {
        self.module
    }

    pub const fn param<T: Rpc>(self, name: &str) -> Self {
        let params = self.module.schema.read_u32(self.params_len_off);
        let schema = self
            .module
            .schema
            .write_u32(self.params_len_off, params + 1);

        let end = self.module.end;
        let (schema, ty_off) = schema.write_str(end, name);

        let ty_id = schema.type_id(&T::TY);
        let schema = schema.write_u32(ty_off, ty_id);

        WriteCmd {
            module: WriteModule {
                schema,
                end: ty_off + 4,
                ..self.module
            },
            ..self
        }
    }
}

impl Type {
    const fn size(&self) -> u32 {
        use Type::*;

        let extra = match self {
            Bool | U8 | U16 | U32 | I32 => 0,
            Tuple(items) => items.len() * 4,

            Struct { name, fields } => {
                let (mut i, mut fields_len) = (0, 0);
                while i < fields.len() {
                    let (name, _ty) = &fields[i];

                    fields_len += 4 + name.len() + 4;
                    i += 1;
                }

                4 + name.len() + fields_len
            }

            Array(_of, _count) => 4 + 4,
            Option(_of) => 4,
        };

        1 + extra as u32
    }

    const fn write(&self, schema: Schema, at: u32) -> Schema {
        use Type::*;

        match self {
            Bool => schema.write_u8(at, 0),
            U8 => schema.write_u8(at, 1),
            U16 => schema.write_u8(at, 2),
            U32 => schema.write_u8(at, 3),
            I32 => schema.write_u8(at, 8),

            Tuple(items) => {
                let mut schema = schema.write_u8(at, 4);

                let mut i = 0;
                while i < items.len() {
                    let (next, id) = schema.insert_type(&items[i]);
                    schema = next.write_u32(at + 1 + 4 * i as u32, id);
                    i += 1;
                }

                schema
            }

            Struct { name, fields } => {
                let (mut schema, mut at) = schema.write_u8(at, 5).write_str(at + 1, name);

                let mut i = 0;
                while i < fields.len() {
                    let (name, ty) = &fields[i];

                    let (next_schema, id) = schema.insert_type(ty);
                    let (next_schema, id_offset) = next_schema.write_str(at, name);
                    schema = next_schema.write_u32(id_offset, id);

                    at = id_offset + 4;
                    i += 1;
                }

                schema
            }

            Array(of, count) => {
                let (schema, id) = schema.insert_type(of);
                schema
                    .write_u8(at, 6)
                    .write_u32(at + 1, id)
                    .write_u32(at + 1 + 4, *count)
            }

            Option(of) => {
                let (schema, id) = schema.insert_type(of);
                schema.write_u8(at, 7).write_u32(at + 1, id)
            }
        }
    }

    const fn check(&self, schema: &Schema, buf: &[u8]) -> bool {
        use Type::*;

        match (self, buf[0]) {
            (Bool, 0) => true,
            (U8, 1) => true,
            (U16, 2) => true,
            (U32, 3) => true,
            (I32, 8) => true,

            (Tuple(items), 4) => {
                let (mut i, mut ok) = (0, true);
                while i < items.len() {
                    if read_u32(buf, 1 + 4 * i) != schema.type_id(&items[i]) {
                        ok = false;
                        break;
                    }

                    i += 1;
                }

                ok
            }

            (Struct { name, fields }, 5) => {
                let mut ok = {
                    let name_len = read_u32(buf, 1) as usize;
                    let name_bytes = slice_range(buf, 1 + 4, 1 + 4 + name.len());

                    name_len == name.len() && eq_bytes(name.as_bytes(), name_bytes)
                };

                let (mut i, mut at) = (0, 1 + 4 + name.len());
                while ok && i < fields.len() {
                    let (name, ty) = &fields[i];

                    let name_len = read_u32(buf, at) as usize;
                    at += 4;

                    let name_bytes = slice_range(buf, at, at + name.len());
                    at += name.len();

                    let id = read_u32(buf, at);
                    ok = name_len == name.len()
                        && eq_bytes(name.as_bytes(), name_bytes)
                        && id == schema.type_id(ty);

                    i += 1;
                    at += 4;
                }

                ok
            }

            (Array(of, count), 6) => {
                let actual_of = read_u32(buf, 1);
                let actual_count = read_u32(buf, 1 + 4);

                actual_of == schema.type_id(*of) && actual_count == *count
            }

            (Option(of), 7) => {
                let actual_of = read_u32(buf, 1);
                actual_of == schema.type_id(*of)
            }

            _ => false,
        }
    }
}

impl Schema {
    const HDR_VER_MAJOR: u32 = 0;
    const HDR_VER_MINOR: u32 = 1;
    const HDR_VER_PATCH: u32 = 2;
    const HDR_MODS: u32 = 4;
    const HDR_END: u32 = 8;
    const HDR_SIZEOF: u32 = 12;

    pub const fn new() -> Self {
        (Schema { buf: [0; MEM_MAX] })
            .write_u8(Self::HDR_VER_MAJOR, ProtoVersion::BUILT.major)
            .write_u8(Self::HDR_VER_MINOR, ProtoVersion::BUILT.minor)
            .write_u8(Self::HDR_VER_PATCH, ProtoVersion::BUILT.patch)
            .write_u32(Self::HDR_MODS, Self::HDR_SIZEOF as u32)
            .write_u32(Self::HDR_END, Self::HDR_SIZEOF as u32)
    }

    pub const fn crc32(&self) -> u32 {
        let len = self.read_u32(Self::HDR_END) as usize;
        Crc::<u32>::new(&CRC_32_CKSUM).checksum(slice_up_to(&self.buf, len))
    }

    pub const fn module(self, name: &str) -> WriteModule {
        let name_off = self.read_u32(Self::HDR_END);
        let (schema, cmds_len_off) = self.write_str(name_off, name);

        WriteModule {
            schema,
            cmds_len_off,
            end: cmds_len_off + 4,
        }
    }

    pub const fn put_type<T: Rpc>(self) -> Self {
        self.insert_type(&T::TY).0
    }

    const fn insert_type(mut self, ty: &Type) -> (Self, u32) {
        let size = ty.size();

        let id = self.find_type(ty, size);
        if id != 0 {
            return (self, id);
        }

        let start = self.read_u32(Self::HDR_END);
        let end = start + 4 + size;

        self = self
            .write_u32(Self::HDR_MODS, end)
            .write_u32(Self::HDR_END, end)
            .write_u32(start, size);

        (ty.write(self, start + 4), start)
    }

    const fn type_id(&self, ty: &Type) -> u32 {
        self.find_type(ty, ty.size())
    }

    const fn find_type(&self, ty: &Type, size: u32) -> u32 {
        let mut start = Self::HDR_SIZEOF;
        let tys_end = self.read_u32(Self::HDR_MODS);

        while start < tys_end {
            let len = self.read_u32(start);
            let end = start + 4 + len;

            let inner = slice_range(&self.buf, start as usize + 4, end as usize);
            if len == size && ty.check(self, inner) {
                return start;
            }

            start = end;
        }

        0
    }

    const fn read_u32(&self, at: u32) -> u32 {
        read_u32(&self.buf, at as usize)
    }

    const fn write_u8(mut self, at: u32, val: u8) -> Self {
        self.buf[at as usize] = val;
        self
    }

    const fn write_u32(mut self, at: u32, val: u32) -> Self {
        let at = at as usize;

        self.buf[at] = val as u8;
        self.buf[at + 1] = (val >> 8) as u8;
        self.buf[at + 2] = (val >> 16) as u8;
        self.buf[at + 3] = (val >> 24) as u8;

        self
    }

    const fn write_str(mut self, at: u32, data: &str) -> (Self, u32) {
        self = self
            .write_u32(at, data.len() as u32)
            .write(at + 4, data.as_bytes());

        (self, at + 4 + data.len() as u32)
    }

    const fn write(mut self, at: u32, data: &[u8]) -> Self {
        let mut i = 0;
        while i < data.len() {
            self.buf[at as usize + i] = data[i];
            i += 1;
        }

        self
    }
}

const MEM_MAX: usize = 16384;

const fn read_u32(buf: &[u8], at: usize) -> u32 {
    buf[at] as u32
        | (buf[at + 1] as u32) << 8
        | (buf[at + 2] as u32) << 16
        | (buf[at + 3] as u32) << 24
}
