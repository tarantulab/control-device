#![no_std]

use core::fmt;
use konst::{primitive::parse_u8, unwrap_ctx};
use serde::{Deserialize, Serialize};

mod schema;
pub use schema::*;

#[derive(Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ProtoVersion {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
}

#[derive(Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DeviceInit {
    pub crc32: u32,
}

#[derive(Serialize, Deserialize)]
pub struct Msg<B> {
    pub unique: u32,
    pub flags: u8,
    pub errno: u8,
    pub body: B,
}

impl ProtoVersion {
    pub const BUILT: Self = ProtoVersion {
        major: unwrap_ctx!(parse_u8(built_info::PKG_VERSION_MAJOR)),
        minor: unwrap_ctx!(parse_u8(built_info::PKG_VERSION_MINOR)),
        patch: unwrap_ctx!(parse_u8(built_info::PKG_VERSION_PATCH)),
    };
}

impl fmt::Display for ProtoVersion {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

impl<B> Msg<B> {
    pub fn zeroed(body: B) -> Self {
        Msg {
            unique: 0,
            flags: 0,
            errno: 0,
            body,
        }
    }

    pub fn split(self) -> (Msg<()>, B) {
        let header = Msg {
            unique: self.unique,
            flags: self.flags,
            errno: self.errno,
            body: (),
        };

        (header, self.body)
    }
}

impl Msg<()> {
    pub fn attach<B>(self, body: B) -> Msg<B> {
        Msg {
            unique: self.unique,
            flags: self.flags,
            errno: self.errno,
            body,
        }
    }
}

mod built_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}
